package com.latam.ejemplo.client;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.latam.ejemplo.domain.ValidateFormOfPaymentFraudRQ;
import com.latam.ejemplo.domain.ValidateFormOfPaymentFraudRS;
import com.latam.ejemplo.exception.ClientWebServiceException;
import com.latam.ejemplo.utils.XmlTools;

/**
 * @author G.Carcamo Clase TEST, en esta clase se realiza una llamada hacia
 *         servicio Soap indicado en su cliente. Pruebas generadas con MockUp
 *         para servicio ValidateFormOfPaymentFraud.
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class SoapClientTest {

	// G.Carcamo - @Autowired se utiliza para generar cliente de servicio en
	// contexto de lo que se implementó
	@Autowired
	SoapClientBR clienteSoap;

	// G.Carcamo - Dado @Test se puede ejecutar metodo como prueba JUnit.
	@Test
	public void validateSoap() throws ClientWebServiceException {

		final Logger logger = LoggerFactory.getLogger(SoapClientTest.class);

		ValidateFormOfPaymentFraudRQ request = new ValidateFormOfPaymentFraudRQ();

		ValidateFormOfPaymentFraudRS response = clienteSoap.getResponse(request);
		// G.Carcamo - Se imprime respuesta de servicio en validacion de conexión
		// correcta.
		logger.info("Response" + XmlTools.toXml(response));
	}
}
