package com.latam.ejemplo.commons;

public final class GlobalConstant {

	public final static String WS_URL_TAM_CYBERSOURCE_PROPERTIES = "ws.tam.url.cybersource";
	public final static String WS_SECURITY_TAM_BR_USERNAME = "ws.tam.security.username.br";
	public final static String WS_SECURITY_TAM_INT_USERNAME = "ws.tam.security.username.int";
	public final static String WS_SECURITY_TAM_BR_PASSWORD_BASE64_TEXT = "ws.tam.security.password.base64text.br";
	public final static String WS_SECURITY_TAM_INT_PASSWORD_BASE64_TEXT = "ws.tam.security.password.base64text.int";
	
    //TODO Generar constantes para version de servico Chile , ademas de agregar properties key
	
	public final static String WS_ENDPOINT_TAM_CYBERSOURCE_PROPERTIES = "ws.tam.endpoint.cybersource";
	public final static String WS_MERCHANT_ID_PROPERTIES = "merchantid.br";
	
	//Se setean constantes para QNAME
	public final static String WS_QNAME_ATTRIBUTE_ONE = "urn:schemas-cybersource-com:transaction-data:TransactionProcessor";
	public final static String WS_QNAME_ATTRIBUTE_TWO  = "TransactionProcessor";
	
}
