package com.latam.ejemplo.client;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

/**
 * @author G.Carcamo 
 * Generacion de clase de configuracion asociado a Cliente Soap. 
 * Encargado de definir un marshall para efectos de procesamiento.
 */
@Configuration
public class SoapConfig {

	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		//Dada la generacion de clases de dominio con plugin JaxB, se debe especificar el package donde se generaron objetos de dominio.
		//This package must match the package in the <generatePackage> specified in pom.xml
		marshaller.setContextPath("com.latam.ejemplo.domain");
		return marshaller;
	}

	@Bean
	public SoapClientBR testClient(Jaxb2Marshaller marshaller) {
		SoapClientBR testClient = new SoapClientBR();
		testClient.setDefaultUri("http://san-dx71fh2:8090/mockValidate");
		testClient.setMarshaller(marshaller);
		testClient.setUnmarshaller(marshaller);
		return testClient;
	}

}
