package com.latam.ejemplo.client;

import org.springframework.context.annotation.Bean;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import com.latam.ejemplo.domain.ValidateFormOfPaymentFraudRQ;
import com.latam.ejemplo.domain.ValidateFormOfPaymentFraudRS;
import com.latam.ejemplo.exception.ClientWebServiceException;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//G.Carcamo - Importaciones comentadas corresponden a uso de cliente anterior.

//import org.springframework.ws.soap.client.core.SoapActionCallback;
//import java.net.MalformedURLException;
//import java.net.URL;
//import java.util.ArrayList;
//import java.util.List;
//import javax.security.auth.login.AppConfigurationEntry;
//import javax.xml.namespace.QName;
//import javax.xml.ws.BindingProvider;
//import javax.xml.ws.handler.*;
//import com.latam.ejemplo.commons.GlobalConstant;
//import com.lan.webservice.jaxws.cybersource.validateFraud.ITransactionProcessor;
//import com.lan.webservice.jaxws.cybersource.validateFraud.TransactionProcessor;
//import com.latam.ejemplo.client.handler.WSSUsernameTokenSecurityHandler;

/**
 * @author G.carcamo
 * Controlador de mensajes Soap, se halla documentacion para su generacion mas sencilla
 * Parametros para llamada ( Uri , Request , WebServiceMessageCallback [Objeto que indica modificaicones a RQ, agregando headers , entre otros])
 */
public class SoapClientBR extends WebServiceGatewaySupport {
	Properties appConfig = new Properties();
	private static final Logger logger = LoggerFactory.getLogger(SoapClientBR.class);
	@Bean 
	public ValidateFormOfPaymentFraudRS getResponse(ValidateFormOfPaymentFraudRQ requestRest) throws ClientWebServiceException {
		
		//G.Cárcamo - Se crea instancia de objeto RQ de dominio, se enviará vacio dada pruebas con Mock-Up SOAP		
		ValidateFormOfPaymentFraudRQ request = new ValidateFormOfPaymentFraudRQ();
		
		//G.Carcamo - En caso de requerirlo, agregar logica y mapeos a request
		request=requestRest;

		logger.info("EndPoint -> http://san-dx71fh2:8090/mockValidate");
		
		//G.Carcamo - Llamada a servicio en espera de response, atencion a SoapRequestHeaderModifier. Esta se debe crear dado ejemplo.
		ValidateFormOfPaymentFraudRS response = (ValidateFormOfPaymentFraudRS) getWebServiceTemplate()
				.marshalSendAndReceive("http://san-dx71fh2:8090/mockValidate", request, new SoapRequestHeaderModifier());
		
		return response;
	}
	
// G.Carcamo - Se comenta implementacion previa de Cliente, este de tipo similar a los utilizados en ALLEGRO-LATAM	
	
//	private static final Logger logger = LoggerFactory.getLogger(SoapClientBR.class);
//
//	// TODO añadir puerto de llamada a servicio soap
//	ITransactionProcessor port;
//	private String userService;
//	private String passService;
//
//	// Constructor
//
//	public SoapClientBR(Properties appConfig, String merchanId) throws Exception {
//		try {
//			URL urlWsdl = new URL(appConfig.getProperty(GlobalConstant.WS_URL_TAM_CYBERSOURCE_PROPERTIES));
//			setUserService(appConfig.getProperty(GlobalConstant.WS_MERCHANT_ID_PROPERTIES).equalsIgnoreCase(merchanId)
//					? appConfig.getProperty(GlobalConstant.WS_SECURITY_TAM_BR_USERNAME)
//					: appConfig.getProperty(GlobalConstant.WS_SECURITY_TAM_INT_USERNAME));
//			setPassService(appConfig.getProperty(GlobalConstant.WS_MERCHANT_ID_PROPERTIES).equalsIgnoreCase(merchanId)
//					? appConfig.getProperty(GlobalConstant.WS_SECURITY_TAM_BR_PASSWORD_BASE64_TEXT)
//					: appConfig.getProperty(GlobalConstant.WS_SECURITY_TAM_INT_PASSWORD_BASE64_TEXT));
//			QName serviceName = new QName(GlobalConstant.WS_QNAME_ATTRIBUTE_ONE,GlobalConstant.WS_QNAME_ATTRIBUTE_TWO);
//			TransactionProcessor transactionProcessor = new TransactionProcessor(urlWsdl, serviceName);
//
//			// se agregan las cabceras necesarias del servicio
//			transactionProcessor.setHandlerResolver(new HandlerResolver() {
//				@SuppressWarnings("rawtypes")
//				@Override
//				public List<Handler> getHandlerChain(PortInfo portInfo) {
//					List<Handler> handlerList = new ArrayList<Handler>();
//					handlerList.add(new WSSUsernameTokenSecurityHandler(userService, passService));
//					return handlerList;
//				}
//			});
//
//			port = transactionProcessor.getPortXML();
//			((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
//					appConfig.getProperty(GlobalConstant.WS_ENDPOINT_TAM_CYBERSOURCE_PROPERTIES));
//
//		} catch (MalformedURLException e) {
//			if (logger.isErrorEnabled()) {
//				logger.error(e.getMessage() + " :" + e);
//			}
//			throw new ClientWebServiceException(e.getMessage(), null);
//		}
//	}
//
//	public String getUserService() {
//		return userService;
//	}
//
//	public void setUserService(String userService) {
//		this.userService = userService;
//	}
//
//	public String getPassService() {
//		return passService;
//	}
//
//	public void setPassService(String passService) {
//		this.passService = passService;
//	}

}
