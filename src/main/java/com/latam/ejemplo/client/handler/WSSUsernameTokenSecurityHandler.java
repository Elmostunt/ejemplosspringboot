package com.latam.ejemplo.client.handler;

import java.util.Set;
import java.util.TreeSet;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPHeader;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class allows you to dynamically security service invokes
 * 
 * @author cvargasm
 * 
 */
public class WSSUsernameTokenSecurityHandler implements
		SOAPHandler<SOAPMessageContext> {

	private String user;
	private String pass;
	private static final Logger logger = LoggerFactory
			.getLogger(WSSUsernameTokenSecurityHandler.class);

	public WSSUsernameTokenSecurityHandler(String user, String pass) {
		setUser(user);
		setPass(pass);
	}

	@Override
	public boolean handleMessage(SOAPMessageContext context) {
		Boolean outboundProperty = (Boolean) context
				.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		if (outboundProperty.booleanValue()) {
			try {
				SOAPEnvelope envelope = context.getMessage().getSOAPPart()
						.getEnvelope();
				SOAPFactory factory = SOAPFactory.newInstance();
				String prefix = "wsse";
				String uri = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
				SOAPElement securityElem = factory.createElement("Security",
						prefix, uri);
				SOAPElement tokenElem = factory.createElement("UsernameToken",
						prefix, uri);
				tokenElem.addAttribute(QName.valueOf("wsu:Id"),
						"UsernameToken-2");
				tokenElem
						.addAttribute(
								QName.valueOf("xmlns:wsu"),
								"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
				SOAPElement userElem = factory.createElement("Username",
						prefix, uri);
				userElem.addTextNode(getUser());
				SOAPElement pwdElem = factory.createElement("Password", prefix,
						uri);
				pwdElem.addTextNode(getPass());
				pwdElem.addAttribute(
						QName.valueOf("Type"),
						"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
				tokenElem.addChildElement(userElem);
				tokenElem.addChildElement(pwdElem);
				securityElem.addChildElement(tokenElem);
				if (envelope.getHeader() != null) {
					envelope.getHeader().detachNode();
				}

				SOAPHeader header = envelope.addHeader();
				header.addChildElement(securityElem);
				
				logger.info("** ResponseHandler: " + context.getMessage().toString());

			} catch (SOAPException e) {
				if (logger.isErrorEnabled()) {
					logger.error("Exception in SOAPException ", e);
				}
			}
		}
		return true;
	}

	@Override
	public Set<QName> getHeaders() {
		return new TreeSet<QName>();
	}

	@Override
	public boolean handleFault(SOAPMessageContext context) {
		return false;
	}

	@Override
	public void close(MessageContext context) {
		//
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

}
