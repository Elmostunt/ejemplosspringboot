package com.latam.ejemplo.client;

import java.io.IOException;
import javax.xml.soap.MimeHeaders;
import javax.xml.transform.TransformerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.soap.saaj.SaajSoapMessage;

/**
 * @author G.Carcamo
 * Clase utilitaria anexa a cliente, en esta se definen los Headers previa llamada a servicio.
 */

public class SoapRequestHeaderModifier implements WebServiceMessageCallback{

	final Logger logger = LoggerFactory.getLogger(SoapRequestHeaderModifier.class);
	//G.Carcamo - Se definen nombre y valor de Header. Dado analisis VeracodeGreenLight, basta mejorar seguridad en seteo de esta.
	
	private final String userName = "LAN-ApplicationName";
	private final String passWd = "ALLEGRO";

	@Override
	public void doWithMessage(WebServiceMessage message) throws IOException, TransformerException {
		if (message instanceof SaajSoapMessage) {
			   SaajSoapMessage soapMessage = (SaajSoapMessage) message;
			   MimeHeaders mimeHeader = soapMessage.getSaajMessage().getMimeHeaders();
			   mimeHeader.setHeader(userName, passWd );
		}
	}
	
	//G.Cárcamo - En caso de requerir autenticacion "Authorization" el value del header se codifica 
//	private String getB64Auth(String login, String pass) {
//		  String source = login + ":" + pass;
//		  String retunVal = "Basic " + Base64.getUrlEncoder().encodeToString(source.getBytes());
//		  return retunVal;
//		 }

}
