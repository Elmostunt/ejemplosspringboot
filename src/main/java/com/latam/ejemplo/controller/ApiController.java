package com.latam.ejemplo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.latam.ejemplo.domain.ExampleObject;
import com.latam.ejemplo.domain.ValidateFormOfPaymentFraudRQ;
import com.latam.ejemplo.domain.ValidateFormOfPaymentFraudRS;

/**
 * @author G.Carcamo
 * Clase de controladores REST.
 * El primero gestiona las peticiones JSON, mapeando a objetos RQ y RS del servicio. Se apunta a localhost:8081/controller.
 * El segundo realiza peticiones JSON , al validar la entrada responde String. Este fue utilizado con postman, enviandole ejemplos JSON a localhost:8081/string.
 */
@RestController
public class ApiController {
	@RequestMapping(value = "/controller", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ValidateFormOfPaymentFraudRS> obtainCybersourceResponse(
			@RequestBody ValidateFormOfPaymentFraudRQ validateFormOfPaymentRQ) {

		ValidateFormOfPaymentFraudRS response = new ValidateFormOfPaymentFraudRS();

		return new ResponseEntity<ValidateFormOfPaymentFraudRS>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/string", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<String> stringBridge(@RequestBody ExampleObject bridge) {
		String response;
		if(bridge.getMessage().isEmpty()) {
			response = "OK";
		}else {
			response ="No OK";
		}
		System.out.println("Entre Aqui!");
		return new ResponseEntity<String>(response, HttpStatus.OK);
	}

}
