//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2018.10.02 a las 01:22:49 PM CLST 
//


package com.latam.ejemplo.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Entidad de negocio que representa los datos basicos de un tramo de un vuelo.
 * 
 * <p>Clase Java para FlightLegType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="FlightLegType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="airlineCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="flightNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="departureAirport" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="departureDateTime" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="arrivalAirport" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="statusLeg" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="fareClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="countryDeparture" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="countryArrival" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="departureDateTimeOriginal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlightLegType", propOrder = {
    "airlineCode",
    "flightNumber",
    "departureAirport",
    "departureDateTime",
    "arrivalAirport",
    "statusLeg",
    "fareClass",
    "countryDeparture",
    "countryArrival",
    "departureDateTimeOriginal"
})
public class FlightLegType {

    @XmlElement(required = true)
    protected String airlineCode;
    @XmlElement(required = true)
    protected String flightNumber;
    @XmlElement(required = true)
    protected String departureAirport;
    @XmlElement(required = true)
    protected String departureDateTime;
    protected String arrivalAirport;
    @XmlElement(required = true)
    protected String statusLeg;
    protected String fareClass;
    protected String countryDeparture;
    protected String countryArrival;
    protected String departureDateTimeOriginal;

    /**
     * Obtiene el valor de la propiedad airlineCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirlineCode() {
        return airlineCode;
    }

    /**
     * Define el valor de la propiedad airlineCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirlineCode(String value) {
        this.airlineCode = value;
    }

    /**
     * Obtiene el valor de la propiedad flightNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * Define el valor de la propiedad flightNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightNumber(String value) {
        this.flightNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad departureAirport.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartureAirport() {
        return departureAirport;
    }

    /**
     * Define el valor de la propiedad departureAirport.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartureAirport(String value) {
        this.departureAirport = value;
    }

    /**
     * Obtiene el valor de la propiedad departureDateTime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartureDateTime() {
        return departureDateTime;
    }

    /**
     * Define el valor de la propiedad departureDateTime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartureDateTime(String value) {
        this.departureDateTime = value;
    }

    /**
     * Obtiene el valor de la propiedad arrivalAirport.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArrivalAirport() {
        return arrivalAirport;
    }

    /**
     * Define el valor de la propiedad arrivalAirport.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArrivalAirport(String value) {
        this.arrivalAirport = value;
    }

    /**
     * Obtiene el valor de la propiedad statusLeg.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusLeg() {
        return statusLeg;
    }

    /**
     * Define el valor de la propiedad statusLeg.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusLeg(String value) {
        this.statusLeg = value;
    }

    /**
     * Obtiene el valor de la propiedad fareClass.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFareClass() {
        return fareClass;
    }

    /**
     * Define el valor de la propiedad fareClass.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFareClass(String value) {
        this.fareClass = value;
    }

    /**
     * Obtiene el valor de la propiedad countryDeparture.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryDeparture() {
        return countryDeparture;
    }

    /**
     * Define el valor de la propiedad countryDeparture.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryDeparture(String value) {
        this.countryDeparture = value;
    }

    /**
     * Obtiene el valor de la propiedad countryArrival.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryArrival() {
        return countryArrival;
    }

    /**
     * Define el valor de la propiedad countryArrival.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryArrival(String value) {
        this.countryArrival = value;
    }

    /**
     * Obtiene el valor de la propiedad departureDateTimeOriginal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartureDateTimeOriginal() {
        return departureDateTimeOriginal;
    }

    /**
     * Define el valor de la propiedad departureDateTimeOriginal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartureDateTimeOriginal(String value) {
        this.departureDateTimeOriginal = value;
    }

}
