//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2018.10.02 a las 01:22:49 PM CLST 
//


package com.latam.ejemplo.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ServiceStatus" type="{http://entities.latam.com/v3_0}ServiceStatusType"/&gt;
 *         &lt;element name="ValidationResult" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="isValid" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *                   &lt;element name="validationRemark" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="requestId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "serviceStatus",
    "validationResult"
})
@XmlRootElement(name = "ValidateFormOfPaymentFraudRS", namespace = "http://ws.latam.com/v3_0")
public class ValidateFormOfPaymentFraudRS {

    @XmlElement(name = "ServiceStatus", required = true)
    protected ServiceStatusType serviceStatus;
    @XmlElement(name = "ValidationResult")
    protected ValidateFormOfPaymentFraudRS.ValidationResult validationResult;

    /**
     * Obtiene el valor de la propiedad serviceStatus.
     * 
     * @return
     *     possible object is
     *     {@link ServiceStatusType }
     *     
     */
    public ServiceStatusType getServiceStatus() {
        return serviceStatus;
    }

    /**
     * Define el valor de la propiedad serviceStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceStatusType }
     *     
     */
    public void setServiceStatus(ServiceStatusType value) {
        this.serviceStatus = value;
    }

    /**
     * Obtiene el valor de la propiedad validationResult.
     * 
     * @return
     *     possible object is
     *     {@link ValidateFormOfPaymentFraudRS.ValidationResult }
     *     
     */
    public ValidateFormOfPaymentFraudRS.ValidationResult getValidationResult() {
        return validationResult;
    }

    /**
     * Define el valor de la propiedad validationResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidateFormOfPaymentFraudRS.ValidationResult }
     *     
     */
    public void setValidationResult(ValidateFormOfPaymentFraudRS.ValidationResult value) {
        this.validationResult = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="isValid" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
     *         &lt;element name="validationRemark" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="requestId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "isValid",
        "validationRemark",
        "requestId"
    })
    public static class ValidationResult {

        protected boolean isValid;
        @XmlElement(required = true)
        protected String validationRemark;
        @XmlElement(required = true)
        protected String requestId;

        /**
         * Obtiene el valor de la propiedad isValid.
         * 
         */
        public boolean isIsValid() {
            return isValid;
        }

        /**
         * Define el valor de la propiedad isValid.
         * 
         */
        public void setIsValid(boolean value) {
            this.isValid = value;
        }

        /**
         * Obtiene el valor de la propiedad validationRemark.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValidationRemark() {
            return validationRemark;
        }

        /**
         * Define el valor de la propiedad validationRemark.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValidationRemark(String value) {
            this.validationRemark = value;
        }

        /**
         * Obtiene el valor de la propiedad requestId.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRequestId() {
            return requestId;
        }

        /**
         * Define el valor de la propiedad requestId.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRequestId(String value) {
            this.requestId = value;
        }

    }

}
