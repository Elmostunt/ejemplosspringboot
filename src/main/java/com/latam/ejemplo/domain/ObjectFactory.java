//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2018.10.02 a las 01:22:49 PM CLST 
//


package com.latam.ejemplo.domain;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.latam.ejemplo.domain package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.latam.ejemplo.domain
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ValidateFormOfPaymentFraudRQ }
     * 
     */
    public ValidateFormOfPaymentFraudRQ createValidateFormOfPaymentFraudRQ() {
        return new ValidateFormOfPaymentFraudRQ();
    }

    /**
     * Create an instance of {@link ValidateFormOfPaymentFraudRS }
     * 
     */
    public ValidateFormOfPaymentFraudRS createValidateFormOfPaymentFraudRS() {
        return new ValidateFormOfPaymentFraudRS();
    }

    /**
     * Create an instance of {@link PassengerBookingType }
     * 
     */
    public PassengerBookingType createPassengerBookingType() {
        return new PassengerBookingType();
    }

    /**
     * Create an instance of {@link PassengerBookingType.Documents }
     * 
     */
    public PassengerBookingType.Documents createPassengerBookingTypeDocuments() {
        return new PassengerBookingType.Documents();
    }

    /**
     * Create an instance of {@link ValidateFormOfPaymentFraudRQ.PayerInformation }
     * 
     */
    public ValidateFormOfPaymentFraudRQ.PayerInformation createValidateFormOfPaymentFraudRQPayerInformation() {
        return new ValidateFormOfPaymentFraudRQ.PayerInformation();
    }

    /**
     * Create an instance of {@link ValidateFormOfPaymentFraudRQ.PayerInformation.MemberInformations }
     * 
     */
    public ValidateFormOfPaymentFraudRQ.PayerInformation.MemberInformations createValidateFormOfPaymentFraudRQPayerInformationMemberInformations() {
        return new ValidateFormOfPaymentFraudRQ.PayerInformation.MemberInformations();
    }

    /**
     * Create an instance of {@link ValidateFormOfPaymentFraudRQ.SaleInformation }
     * 
     */
    public ValidateFormOfPaymentFraudRQ.SaleInformation createValidateFormOfPaymentFraudRQSaleInformation() {
        return new ValidateFormOfPaymentFraudRQ.SaleInformation();
    }

    /**
     * Create an instance of {@link ValidateFormOfPaymentFraudRQ.SaleInformation.FormOfPaymentInformation }
     * 
     */
    public ValidateFormOfPaymentFraudRQ.SaleInformation.FormOfPaymentInformation createValidateFormOfPaymentFraudRQSaleInformationFormOfPaymentInformation() {
        return new ValidateFormOfPaymentFraudRQ.SaleInformation.FormOfPaymentInformation();
    }

    /**
     * Create an instance of {@link ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation }
     * 
     */
    public ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation createValidateFormOfPaymentFraudRQSaleInformationShoppingCartInformation() {
        return new ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation();
    }

    /**
     * Create an instance of {@link ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items }
     * 
     */
    public ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items createValidateFormOfPaymentFraudRQSaleInformationShoppingCartInformationItems() {
        return new ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items();
    }

    /**
     * Create an instance of {@link ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item }
     * 
     */
    public ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item createValidateFormOfPaymentFraudRQSaleInformationShoppingCartInformationItemsItem() {
        return new ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item();
    }

    /**
     * Create an instance of {@link ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item.PassengerInformation }
     * 
     */
    public ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item.PassengerInformation createValidateFormOfPaymentFraudRQSaleInformationShoppingCartInformationItemsItemPassengerInformation() {
        return new ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item.PassengerInformation();
    }

    /**
     * Create an instance of {@link ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item.PassengerInformation.MemberInformations }
     * 
     */
    public ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item.PassengerInformation.MemberInformations createValidateFormOfPaymentFraudRQSaleInformationShoppingCartInformationItemsItemPassengerInformationMemberInformations() {
        return new ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item.PassengerInformation.MemberInformations();
    }

    /**
     * Create an instance of {@link ValidateFormOfPaymentFraudRQ.CardInformation }
     * 
     */
    public ValidateFormOfPaymentFraudRQ.CardInformation createValidateFormOfPaymentFraudRQCardInformation() {
        return new ValidateFormOfPaymentFraudRQ.CardInformation();
    }

    /**
     * Create an instance of {@link ValidateFormOfPaymentFraudRQ.MarketInformation }
     * 
     */
    public ValidateFormOfPaymentFraudRQ.MarketInformation createValidateFormOfPaymentFraudRQMarketInformation() {
        return new ValidateFormOfPaymentFraudRQ.MarketInformation();
    }

    /**
     * Create an instance of {@link ValidateFormOfPaymentFraudRQ.Legs }
     * 
     */
    public ValidateFormOfPaymentFraudRQ.Legs createValidateFormOfPaymentFraudRQLegs() {
        return new ValidateFormOfPaymentFraudRQ.Legs();
    }

    /**
     * Create an instance of {@link ServiceStatusType }
     * 
     */
    public ServiceStatusType createServiceStatusType() {
        return new ServiceStatusType();
    }

    /**
     * Create an instance of {@link ValidateFormOfPaymentFraudRS.ValidationResult }
     * 
     */
    public ValidateFormOfPaymentFraudRS.ValidationResult createValidateFormOfPaymentFraudRSValidationResult() {
        return new ValidateFormOfPaymentFraudRS.ValidationResult();
    }

    /**
     * Create an instance of {@link NameType }
     * 
     */
    public NameType createNameType() {
        return new NameType();
    }

    /**
     * Create an instance of {@link IdentificationType }
     * 
     */
    public IdentificationType createIdentificationType() {
        return new IdentificationType();
    }

    /**
     * Create an instance of {@link FlightLegType }
     * 
     */
    public FlightLegType createFlightLegType() {
        return new FlightLegType();
    }

    /**
     * Create an instance of {@link PassengerBookingType.Documents.Document }
     * 
     */
    public PassengerBookingType.Documents.Document createPassengerBookingTypeDocumentsDocument() {
        return new PassengerBookingType.Documents.Document();
    }

    /**
     * Create an instance of {@link ValidateFormOfPaymentFraudRQ.PayerInformation.AddressInformation }
     * 
     */
    public ValidateFormOfPaymentFraudRQ.PayerInformation.AddressInformation createValidateFormOfPaymentFraudRQPayerInformationAddressInformation() {
        return new ValidateFormOfPaymentFraudRQ.PayerInformation.AddressInformation();
    }

    /**
     * Create an instance of {@link ValidateFormOfPaymentFraudRQ.PayerInformation.MemberInformations.MemberInformation }
     * 
     */
    public ValidateFormOfPaymentFraudRQ.PayerInformation.MemberInformations.MemberInformation createValidateFormOfPaymentFraudRQPayerInformationMemberInformationsMemberInformation() {
        return new ValidateFormOfPaymentFraudRQ.PayerInformation.MemberInformations.MemberInformation();
    }

    /**
     * Create an instance of {@link ValidateFormOfPaymentFraudRQ.SaleInformation.SalesOfficeInformation }
     * 
     */
    public ValidateFormOfPaymentFraudRQ.SaleInformation.SalesOfficeInformation createValidateFormOfPaymentFraudRQSaleInformationSalesOfficeInformation() {
        return new ValidateFormOfPaymentFraudRQ.SaleInformation.SalesOfficeInformation();
    }

    /**
     * Create an instance of {@link ValidateFormOfPaymentFraudRQ.SaleInformation.IssueInformation }
     * 
     */
    public ValidateFormOfPaymentFraudRQ.SaleInformation.IssueInformation createValidateFormOfPaymentFraudRQSaleInformationIssueInformation() {
        return new ValidateFormOfPaymentFraudRQ.SaleInformation.IssueInformation();
    }

    /**
     * Create an instance of {@link ValidateFormOfPaymentFraudRQ.SaleInformation.FormOfPaymentInformation.ExchangeTicketInformation }
     * 
     */
    public ValidateFormOfPaymentFraudRQ.SaleInformation.FormOfPaymentInformation.ExchangeTicketInformation createValidateFormOfPaymentFraudRQSaleInformationFormOfPaymentInformationExchangeTicketInformation() {
        return new ValidateFormOfPaymentFraudRQ.SaleInformation.FormOfPaymentInformation.ExchangeTicketInformation();
    }

    /**
     * Create an instance of {@link ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.GrandTotalAmount }
     * 
     */
    public ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.GrandTotalAmount createValidateFormOfPaymentFraudRQSaleInformationShoppingCartInformationGrandTotalAmount() {
        return new ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.GrandTotalAmount();
    }

    /**
     * Create an instance of {@link ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item.UnitPrice }
     * 
     */
    public ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item.UnitPrice createValidateFormOfPaymentFraudRQSaleInformationShoppingCartInformationItemsItemUnitPrice() {
        return new ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item.UnitPrice();
    }

    /**
     * Create an instance of {@link ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item.PassengerInformation.MemberInformations.MemberInformation }
     * 
     */
    public ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item.PassengerInformation.MemberInformations.MemberInformation createValidateFormOfPaymentFraudRQSaleInformationShoppingCartInformationItemsItemPassengerInformationMemberInformationsMemberInformation() {
        return new ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item.PassengerInformation.MemberInformations.MemberInformation();
    }

}
