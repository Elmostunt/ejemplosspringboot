package com.latam.ejemplo.domain;

public class ExampleObject {

	String message;

	public String getMessage() {
		return message;
	}

	/**
	 * Define el valor de la propiedad message.
	 * 
	 * 
	 */
	public void setMessage(String message) {
		this.message = message;
	}
}
