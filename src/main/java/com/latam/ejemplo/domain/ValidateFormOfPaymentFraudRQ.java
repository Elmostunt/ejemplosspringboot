//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2018.10.02 a las 01:22:49 PM CLST 
//


package com.latam.ejemplo.domain;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SaleInformation"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="ShoppingCartInformation"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="defaultIsoCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="grandTotalAmount"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;simpleContent&gt;
 *                                   &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
 *                                     &lt;attribute name="isoCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                   &lt;/extension&gt;
 *                                 &lt;/simpleContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="fareClass" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="Items"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="Item" maxOccurs="unbounded"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;element name="unitPrice"&gt;
 *                                                   &lt;complexType&gt;
 *                                                     &lt;simpleContent&gt;
 *                                                       &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
 *                                                         &lt;attribute name="isoCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                                       &lt;/extension&gt;
 *                                                     &lt;/simpleContent&gt;
 *                                                   &lt;/complexType&gt;
 *                                                 &lt;/element&gt;
 *                                                 &lt;element name="quantity" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *                                                 &lt;element name="productCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                                 &lt;element name="productName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                                 &lt;element name="PassengerInformation"&gt;
 *                                                   &lt;complexType&gt;
 *                                                     &lt;complexContent&gt;
 *                                                       &lt;extension base="{http://entities.latam.com/v3_0}PassengerBookingType"&gt;
 *                                                         &lt;sequence&gt;
 *                                                           &lt;element name="phoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                                           &lt;element name="secondaryPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                                           &lt;element name="MemberInformations" minOccurs="0"&gt;
 *                                                             &lt;complexType&gt;
 *                                                               &lt;complexContent&gt;
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                                   &lt;sequence&gt;
 *                                                                     &lt;element name="MemberInformation" maxOccurs="unbounded"&gt;
 *                                                                       &lt;complexType&gt;
 *                                                                         &lt;complexContent&gt;
 *                                                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                                             &lt;sequence&gt;
 *                                                                               &lt;element name="memberNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                                                               &lt;element name="loyaltyProgramName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                                                               &lt;element name="memberCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                                                               &lt;element name="admissionDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                                                             &lt;/sequence&gt;
 *                                                                           &lt;/restriction&gt;
 *                                                                         &lt;/complexContent&gt;
 *                                                                       &lt;/complexType&gt;
 *                                                                     &lt;/element&gt;
 *                                                                   &lt;/sequence&gt;
 *                                                                 &lt;/restriction&gt;
 *                                                               &lt;/complexContent&gt;
 *                                                             &lt;/complexType&gt;
 *                                                           &lt;/element&gt;
 *                                                         &lt;/sequence&gt;
 *                                                       &lt;/extension&gt;
 *                                                     &lt;/complexContent&gt;
 *                                                   &lt;/complexType&gt;
 *                                                 &lt;/element&gt;
 *                                               &lt;/sequence&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="FormOfPaymentInformation"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="fopTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="useMultipleFormsOfPayment" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *                             &lt;element name="useRedemptionVoucher" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *                             &lt;element name="ExchangeTicketInformation" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="useMultipleExchangeTickets" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *                                       &lt;element name="issuerIsoCityCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="issueDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="ticketNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="msrNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="msrTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="SalesOfficeInformation"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="stationNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="salesAgentCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="salesOfficeName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="countrySalesOffice" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="pnrCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="travelType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="superPnrCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="salesPortalTransactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="IssueInformation"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="issueDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                           &lt;attribute name="isIssue" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="saleType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="PayerInformation"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Name" type="{http://entities.latam.com/v3_0}NameType"/&gt;
 *                   &lt;element name="Identification" type="{http://entities.latam.com/v3_0}IdentificationType"/&gt;
 *                   &lt;element name="phoneNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="MemberInformations" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="MemberInformation" maxOccurs="unbounded"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="memberNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="loyaltyProgramName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="memberCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="admissionDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="AddressInformation" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="street" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="complementStreet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="city" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="state" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="postalCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="isoCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="CardInformation"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="accountNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="expirationMonth" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *                   &lt;element name="expirationYear" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *                   &lt;element name="cardTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="useInstallmentPayment" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *                   &lt;element name="installmentsNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *                   &lt;element name="installmentValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="isoCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="issuerBankName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="MarketInformation"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="merchantId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="marketId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Legs"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Leg" type="{http://entities.latam.com/v3_0}FlightLegType" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "saleInformation",
    "payerInformation",
    "cardInformation",
    "marketInformation",
    "legs"
})
@XmlRootElement(name = "ValidateFormOfPaymentFraudRQ", namespace = "http://ws.latam.com/v3_0")
public class ValidateFormOfPaymentFraudRQ {

    @XmlElement(name = "SaleInformation", required = true)
    protected ValidateFormOfPaymentFraudRQ.SaleInformation saleInformation;
    @XmlElement(name = "PayerInformation", required = true)
    protected ValidateFormOfPaymentFraudRQ.PayerInformation payerInformation;
    @XmlElement(name = "CardInformation", required = true)
    protected ValidateFormOfPaymentFraudRQ.CardInformation cardInformation;
    @XmlElement(name = "MarketInformation", required = true)
    protected ValidateFormOfPaymentFraudRQ.MarketInformation marketInformation;
    @XmlElement(name = "Legs", required = true)
    protected ValidateFormOfPaymentFraudRQ.Legs legs;

    /**
     * Obtiene el valor de la propiedad saleInformation.
     * 
     * @return
     *     possible object is
     *     {@link ValidateFormOfPaymentFraudRQ.SaleInformation }
     *     
     */
    public ValidateFormOfPaymentFraudRQ.SaleInformation getSaleInformation() {
        return saleInformation;
    }

    /**
     * Define el valor de la propiedad saleInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidateFormOfPaymentFraudRQ.SaleInformation }
     *     
     */
    public void setSaleInformation(ValidateFormOfPaymentFraudRQ.SaleInformation value) {
        this.saleInformation = value;
    }

    /**
     * Obtiene el valor de la propiedad payerInformation.
     * 
     * @return
     *     possible object is
     *     {@link ValidateFormOfPaymentFraudRQ.PayerInformation }
     *     
     */
    public ValidateFormOfPaymentFraudRQ.PayerInformation getPayerInformation() {
        return payerInformation;
    }

    /**
     * Define el valor de la propiedad payerInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidateFormOfPaymentFraudRQ.PayerInformation }
     *     
     */
    public void setPayerInformation(ValidateFormOfPaymentFraudRQ.PayerInformation value) {
        this.payerInformation = value;
    }

    /**
     * Obtiene el valor de la propiedad cardInformation.
     * 
     * @return
     *     possible object is
     *     {@link ValidateFormOfPaymentFraudRQ.CardInformation }
     *     
     */
    public ValidateFormOfPaymentFraudRQ.CardInformation getCardInformation() {
        return cardInformation;
    }

    /**
     * Define el valor de la propiedad cardInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidateFormOfPaymentFraudRQ.CardInformation }
     *     
     */
    public void setCardInformation(ValidateFormOfPaymentFraudRQ.CardInformation value) {
        this.cardInformation = value;
    }

    /**
     * Obtiene el valor de la propiedad marketInformation.
     * 
     * @return
     *     possible object is
     *     {@link ValidateFormOfPaymentFraudRQ.MarketInformation }
     *     
     */
    public ValidateFormOfPaymentFraudRQ.MarketInformation getMarketInformation() {
        return marketInformation;
    }

    /**
     * Define el valor de la propiedad marketInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidateFormOfPaymentFraudRQ.MarketInformation }
     *     
     */
    public void setMarketInformation(ValidateFormOfPaymentFraudRQ.MarketInformation value) {
        this.marketInformation = value;
    }

    /**
     * Obtiene el valor de la propiedad legs.
     * 
     * @return
     *     possible object is
     *     {@link ValidateFormOfPaymentFraudRQ.Legs }
     *     
     */
    public ValidateFormOfPaymentFraudRQ.Legs getLegs() {
        return legs;
    }

    /**
     * Define el valor de la propiedad legs.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidateFormOfPaymentFraudRQ.Legs }
     *     
     */
    public void setLegs(ValidateFormOfPaymentFraudRQ.Legs value) {
        this.legs = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="accountNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="expirationMonth" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
     *         &lt;element name="expirationYear" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
     *         &lt;element name="cardTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="useInstallmentPayment" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
     *         &lt;element name="installmentsNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
     *         &lt;element name="installmentValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="isoCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="issuerBankName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "accountNumber",
        "expirationMonth",
        "expirationYear",
        "cardTypeCode",
        "useInstallmentPayment",
        "installmentsNumber",
        "installmentValue",
        "isoCountryCode",
        "issuerBankName"
    })
    public static class CardInformation {

        @XmlElement(required = true)
        protected String accountNumber;
        protected int expirationMonth;
        protected int expirationYear;
        @XmlElement(required = true)
        protected String cardTypeCode;
        protected Boolean useInstallmentPayment;
        protected Integer installmentsNumber;
        protected String installmentValue;
        @XmlElement(required = true)
        protected String isoCountryCode;
        @XmlElement(required = true)
        protected String issuerBankName;

        /**
         * Obtiene el valor de la propiedad accountNumber.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAccountNumber() {
            return accountNumber;
        }

        /**
         * Define el valor de la propiedad accountNumber.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAccountNumber(String value) {
            this.accountNumber = value;
        }

        /**
         * Obtiene el valor de la propiedad expirationMonth.
         * 
         */
        public int getExpirationMonth() {
            return expirationMonth;
        }

        /**
         * Define el valor de la propiedad expirationMonth.
         * 
         */
        public void setExpirationMonth(int value) {
            this.expirationMonth = value;
        }

        /**
         * Obtiene el valor de la propiedad expirationYear.
         * 
         */
        public int getExpirationYear() {
            return expirationYear;
        }

        /**
         * Define el valor de la propiedad expirationYear.
         * 
         */
        public void setExpirationYear(int value) {
            this.expirationYear = value;
        }

        /**
         * Obtiene el valor de la propiedad cardTypeCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCardTypeCode() {
            return cardTypeCode;
        }

        /**
         * Define el valor de la propiedad cardTypeCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCardTypeCode(String value) {
            this.cardTypeCode = value;
        }

        /**
         * Obtiene el valor de la propiedad useInstallmentPayment.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isUseInstallmentPayment() {
            return useInstallmentPayment;
        }

        /**
         * Define el valor de la propiedad useInstallmentPayment.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setUseInstallmentPayment(Boolean value) {
            this.useInstallmentPayment = value;
        }

        /**
         * Obtiene el valor de la propiedad installmentsNumber.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getInstallmentsNumber() {
            return installmentsNumber;
        }

        /**
         * Define el valor de la propiedad installmentsNumber.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setInstallmentsNumber(Integer value) {
            this.installmentsNumber = value;
        }

        /**
         * Obtiene el valor de la propiedad installmentValue.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInstallmentValue() {
            return installmentValue;
        }

        /**
         * Define el valor de la propiedad installmentValue.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInstallmentValue(String value) {
            this.installmentValue = value;
        }

        /**
         * Obtiene el valor de la propiedad isoCountryCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIsoCountryCode() {
            return isoCountryCode;
        }

        /**
         * Define el valor de la propiedad isoCountryCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIsoCountryCode(String value) {
            this.isoCountryCode = value;
        }

        /**
         * Obtiene el valor de la propiedad issuerBankName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIssuerBankName() {
            return issuerBankName;
        }

        /**
         * Define el valor de la propiedad issuerBankName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIssuerBankName(String value) {
            this.issuerBankName = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Leg" type="{http://entities.latam.com/v3_0}FlightLegType" maxOccurs="unbounded"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "leg"
    })
    public static class Legs {

        @XmlElement(name = "Leg", required = true)
        protected List<FlightLegType> leg;

        /**
         * Gets the value of the leg property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the leg property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getLeg().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link FlightLegType }
         * 
         * 
         */
        public List<FlightLegType> getLeg() {
            if (leg == null) {
                leg = new ArrayList<FlightLegType>();
            }
            return this.leg;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="merchantId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="marketId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "merchantId",
        "marketId"
    })
    public static class MarketInformation {

        @XmlElement(required = true)
        protected String merchantId;
        @XmlElement(required = true)
        protected String marketId;

        /**
         * Obtiene el valor de la propiedad merchantId.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMerchantId() {
            return merchantId;
        }

        /**
         * Define el valor de la propiedad merchantId.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMerchantId(String value) {
            this.merchantId = value;
        }

        /**
         * Obtiene el valor de la propiedad marketId.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMarketId() {
            return marketId;
        }

        /**
         * Define el valor de la propiedad marketId.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMarketId(String value) {
            this.marketId = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Name" type="{http://entities.latam.com/v3_0}NameType"/&gt;
     *         &lt;element name="Identification" type="{http://entities.latam.com/v3_0}IdentificationType"/&gt;
     *         &lt;element name="phoneNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="MemberInformations" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="MemberInformation" maxOccurs="unbounded"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="memberNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="loyaltyProgramName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="memberCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="admissionDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="AddressInformation" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="street" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="complementStreet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="city" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="state" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="postalCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="isoCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "name",
        "identification",
        "phoneNumber",
        "email",
        "memberInformations",
        "addressInformation"
    })
    public static class PayerInformation {

        @XmlElement(name = "Name", required = true)
        protected NameType name;
        @XmlElement(name = "Identification", required = true)
        protected IdentificationType identification;
        @XmlElement(required = true)
        protected String phoneNumber;
        @XmlElement(required = true)
        protected String email;
        @XmlElement(name = "MemberInformations")
        protected ValidateFormOfPaymentFraudRQ.PayerInformation.MemberInformations memberInformations;
        @XmlElement(name = "AddressInformation")
        protected ValidateFormOfPaymentFraudRQ.PayerInformation.AddressInformation addressInformation;

        /**
         * Obtiene el valor de la propiedad name.
         * 
         * @return
         *     possible object is
         *     {@link NameType }
         *     
         */
        public NameType getName() {
            return name;
        }

        /**
         * Define el valor de la propiedad name.
         * 
         * @param value
         *     allowed object is
         *     {@link NameType }
         *     
         */
        public void setName(NameType value) {
            this.name = value;
        }

        /**
         * Obtiene el valor de la propiedad identification.
         * 
         * @return
         *     possible object is
         *     {@link IdentificationType }
         *     
         */
        public IdentificationType getIdentification() {
            return identification;
        }

        /**
         * Define el valor de la propiedad identification.
         * 
         * @param value
         *     allowed object is
         *     {@link IdentificationType }
         *     
         */
        public void setIdentification(IdentificationType value) {
            this.identification = value;
        }

        /**
         * Obtiene el valor de la propiedad phoneNumber.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPhoneNumber() {
            return phoneNumber;
        }

        /**
         * Define el valor de la propiedad phoneNumber.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPhoneNumber(String value) {
            this.phoneNumber = value;
        }

        /**
         * Obtiene el valor de la propiedad email.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEmail() {
            return email;
        }

        /**
         * Define el valor de la propiedad email.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEmail(String value) {
            this.email = value;
        }

        /**
         * Obtiene el valor de la propiedad memberInformations.
         * 
         * @return
         *     possible object is
         *     {@link ValidateFormOfPaymentFraudRQ.PayerInformation.MemberInformations }
         *     
         */
        public ValidateFormOfPaymentFraudRQ.PayerInformation.MemberInformations getMemberInformations() {
            return memberInformations;
        }

        /**
         * Define el valor de la propiedad memberInformations.
         * 
         * @param value
         *     allowed object is
         *     {@link ValidateFormOfPaymentFraudRQ.PayerInformation.MemberInformations }
         *     
         */
        public void setMemberInformations(ValidateFormOfPaymentFraudRQ.PayerInformation.MemberInformations value) {
            this.memberInformations = value;
        }

        /**
         * Obtiene el valor de la propiedad addressInformation.
         * 
         * @return
         *     possible object is
         *     {@link ValidateFormOfPaymentFraudRQ.PayerInformation.AddressInformation }
         *     
         */
        public ValidateFormOfPaymentFraudRQ.PayerInformation.AddressInformation getAddressInformation() {
            return addressInformation;
        }

        /**
         * Define el valor de la propiedad addressInformation.
         * 
         * @param value
         *     allowed object is
         *     {@link ValidateFormOfPaymentFraudRQ.PayerInformation.AddressInformation }
         *     
         */
        public void setAddressInformation(ValidateFormOfPaymentFraudRQ.PayerInformation.AddressInformation value) {
            this.addressInformation = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="street" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="complementStreet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="city" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="state" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="postalCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="isoCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "street",
            "complementStreet",
            "city",
            "state",
            "postalCode",
            "isoCountryCode"
        })
        public static class AddressInformation {

            @XmlElement(required = true)
            protected String street;
            protected String complementStreet;
            @XmlElement(required = true)
            protected String city;
            @XmlElement(required = true)
            protected String state;
            @XmlElement(required = true)
            protected String postalCode;
            @XmlElement(required = true)
            protected String isoCountryCode;

            /**
             * Obtiene el valor de la propiedad street.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStreet() {
                return street;
            }

            /**
             * Define el valor de la propiedad street.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStreet(String value) {
                this.street = value;
            }

            /**
             * Obtiene el valor de la propiedad complementStreet.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getComplementStreet() {
                return complementStreet;
            }

            /**
             * Define el valor de la propiedad complementStreet.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setComplementStreet(String value) {
                this.complementStreet = value;
            }

            /**
             * Obtiene el valor de la propiedad city.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCity() {
                return city;
            }

            /**
             * Define el valor de la propiedad city.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCity(String value) {
                this.city = value;
            }

            /**
             * Obtiene el valor de la propiedad state.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getState() {
                return state;
            }

            /**
             * Define el valor de la propiedad state.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setState(String value) {
                this.state = value;
            }

            /**
             * Obtiene el valor de la propiedad postalCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPostalCode() {
                return postalCode;
            }

            /**
             * Define el valor de la propiedad postalCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPostalCode(String value) {
                this.postalCode = value;
            }

            /**
             * Obtiene el valor de la propiedad isoCountryCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsoCountryCode() {
                return isoCountryCode;
            }

            /**
             * Define el valor de la propiedad isoCountryCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsoCountryCode(String value) {
                this.isoCountryCode = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="MemberInformation" maxOccurs="unbounded"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="memberNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="loyaltyProgramName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="memberCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="admissionDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "memberInformation"
        })
        public static class MemberInformations {

            @XmlElement(name = "MemberInformation", required = true)
            protected List<ValidateFormOfPaymentFraudRQ.PayerInformation.MemberInformations.MemberInformation> memberInformation;

            /**
             * Gets the value of the memberInformation property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the memberInformation property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getMemberInformation().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ValidateFormOfPaymentFraudRQ.PayerInformation.MemberInformations.MemberInformation }
             * 
             * 
             */
            public List<ValidateFormOfPaymentFraudRQ.PayerInformation.MemberInformations.MemberInformation> getMemberInformation() {
                if (memberInformation == null) {
                    memberInformation = new ArrayList<ValidateFormOfPaymentFraudRQ.PayerInformation.MemberInformations.MemberInformation>();
                }
                return this.memberInformation;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="memberNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="loyaltyProgramName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="memberCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="admissionDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "memberNumber",
                "loyaltyProgramName",
                "memberCategory",
                "admissionDate"
            })
            public static class MemberInformation {

                protected String memberNumber;
                @XmlElement(required = true)
                protected String loyaltyProgramName;
                protected String memberCategory;
                protected String admissionDate;

                /**
                 * Obtiene el valor de la propiedad memberNumber.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMemberNumber() {
                    return memberNumber;
                }

                /**
                 * Define el valor de la propiedad memberNumber.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMemberNumber(String value) {
                    this.memberNumber = value;
                }

                /**
                 * Obtiene el valor de la propiedad loyaltyProgramName.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLoyaltyProgramName() {
                    return loyaltyProgramName;
                }

                /**
                 * Define el valor de la propiedad loyaltyProgramName.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLoyaltyProgramName(String value) {
                    this.loyaltyProgramName = value;
                }

                /**
                 * Obtiene el valor de la propiedad memberCategory.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMemberCategory() {
                    return memberCategory;
                }

                /**
                 * Define el valor de la propiedad memberCategory.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMemberCategory(String value) {
                    this.memberCategory = value;
                }

                /**
                 * Obtiene el valor de la propiedad admissionDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAdmissionDate() {
                    return admissionDate;
                }

                /**
                 * Define el valor de la propiedad admissionDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAdmissionDate(String value) {
                    this.admissionDate = value;
                }

            }

        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="ShoppingCartInformation"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="defaultIsoCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="grandTotalAmount"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;simpleContent&gt;
     *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
     *                           &lt;attribute name="isoCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                         &lt;/extension&gt;
     *                       &lt;/simpleContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="fareClass" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="Items"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="Item" maxOccurs="unbounded"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element name="unitPrice"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;simpleContent&gt;
     *                                             &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
     *                                               &lt;attribute name="isoCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                                             &lt;/extension&gt;
     *                                           &lt;/simpleContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                       &lt;element name="quantity" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
     *                                       &lt;element name="productCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                                       &lt;element name="productName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                                       &lt;element name="PassengerInformation"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;extension base="{http://entities.latam.com/v3_0}PassengerBookingType"&gt;
     *                                               &lt;sequence&gt;
     *                                                 &lt;element name="phoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                                                 &lt;element name="secondaryPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                                                 &lt;element name="MemberInformations" minOccurs="0"&gt;
     *                                                   &lt;complexType&gt;
     *                                                     &lt;complexContent&gt;
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                                         &lt;sequence&gt;
     *                                                           &lt;element name="MemberInformation" maxOccurs="unbounded"&gt;
     *                                                             &lt;complexType&gt;
     *                                                               &lt;complexContent&gt;
     *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                                                   &lt;sequence&gt;
     *                                                                     &lt;element name="memberNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                                                                     &lt;element name="loyaltyProgramName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                                                                     &lt;element name="memberCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                                                                     &lt;element name="admissionDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                                                                   &lt;/sequence&gt;
     *                                                                 &lt;/restriction&gt;
     *                                                               &lt;/complexContent&gt;
     *                                                             &lt;/complexType&gt;
     *                                                           &lt;/element&gt;
     *                                                         &lt;/sequence&gt;
     *                                                       &lt;/restriction&gt;
     *                                                     &lt;/complexContent&gt;
     *                                                   &lt;/complexType&gt;
     *                                                 &lt;/element&gt;
     *                                               &lt;/sequence&gt;
     *                                             &lt;/extension&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                     &lt;/sequence&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="FormOfPaymentInformation"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="fopTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="useMultipleFormsOfPayment" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
     *                   &lt;element name="useRedemptionVoucher" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
     *                   &lt;element name="ExchangeTicketInformation" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="useMultipleExchangeTickets" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
     *                             &lt;element name="issuerIsoCityCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="issueDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="ticketNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="msrNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="msrTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="SalesOfficeInformation"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="stationNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="salesAgentCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="salesOfficeName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="countrySalesOffice" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="pnrCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="travelType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="superPnrCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="salesPortalTransactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="IssueInformation"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="issueDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *                 &lt;attribute name="isIssue" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="saleType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "shoppingCartInformation",
        "formOfPaymentInformation",
        "salesOfficeInformation",
        "pnrCode",
        "travelType",
        "superPnrCode",
        "salesPortalTransactionId",
        "issueInformation",
        "saleType"
    })
    public static class SaleInformation {

        @XmlElement(name = "ShoppingCartInformation", required = true)
        protected ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation shoppingCartInformation;
        @XmlElement(name = "FormOfPaymentInformation", required = true)
        protected ValidateFormOfPaymentFraudRQ.SaleInformation.FormOfPaymentInformation formOfPaymentInformation;
        @XmlElement(name = "SalesOfficeInformation", required = true)
        protected ValidateFormOfPaymentFraudRQ.SaleInformation.SalesOfficeInformation salesOfficeInformation;
        @XmlElement(required = true)
        protected String pnrCode;
        protected String travelType;
        protected String superPnrCode;
        protected String salesPortalTransactionId;
        @XmlElement(name = "IssueInformation", required = true)
        protected ValidateFormOfPaymentFraudRQ.SaleInformation.IssueInformation issueInformation;
        protected String saleType;

        /**
         * Obtiene el valor de la propiedad shoppingCartInformation.
         * 
         * @return
         *     possible object is
         *     {@link ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation }
         *     
         */
        public ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation getShoppingCartInformation() {
            return shoppingCartInformation;
        }

        /**
         * Define el valor de la propiedad shoppingCartInformation.
         * 
         * @param value
         *     allowed object is
         *     {@link ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation }
         *     
         */
        public void setShoppingCartInformation(ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation value) {
            this.shoppingCartInformation = value;
        }

        /**
         * Obtiene el valor de la propiedad formOfPaymentInformation.
         * 
         * @return
         *     possible object is
         *     {@link ValidateFormOfPaymentFraudRQ.SaleInformation.FormOfPaymentInformation }
         *     
         */
        public ValidateFormOfPaymentFraudRQ.SaleInformation.FormOfPaymentInformation getFormOfPaymentInformation() {
            return formOfPaymentInformation;
        }

        /**
         * Define el valor de la propiedad formOfPaymentInformation.
         * 
         * @param value
         *     allowed object is
         *     {@link ValidateFormOfPaymentFraudRQ.SaleInformation.FormOfPaymentInformation }
         *     
         */
        public void setFormOfPaymentInformation(ValidateFormOfPaymentFraudRQ.SaleInformation.FormOfPaymentInformation value) {
            this.formOfPaymentInformation = value;
        }

        /**
         * Obtiene el valor de la propiedad salesOfficeInformation.
         * 
         * @return
         *     possible object is
         *     {@link ValidateFormOfPaymentFraudRQ.SaleInformation.SalesOfficeInformation }
         *     
         */
        public ValidateFormOfPaymentFraudRQ.SaleInformation.SalesOfficeInformation getSalesOfficeInformation() {
            return salesOfficeInformation;
        }

        /**
         * Define el valor de la propiedad salesOfficeInformation.
         * 
         * @param value
         *     allowed object is
         *     {@link ValidateFormOfPaymentFraudRQ.SaleInformation.SalesOfficeInformation }
         *     
         */
        public void setSalesOfficeInformation(ValidateFormOfPaymentFraudRQ.SaleInformation.SalesOfficeInformation value) {
            this.salesOfficeInformation = value;
        }

        /**
         * Obtiene el valor de la propiedad pnrCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPnrCode() {
            return pnrCode;
        }

        /**
         * Define el valor de la propiedad pnrCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPnrCode(String value) {
            this.pnrCode = value;
        }

        /**
         * Obtiene el valor de la propiedad travelType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTravelType() {
            return travelType;
        }

        /**
         * Define el valor de la propiedad travelType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTravelType(String value) {
            this.travelType = value;
        }

        /**
         * Obtiene el valor de la propiedad superPnrCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSuperPnrCode() {
            return superPnrCode;
        }

        /**
         * Define el valor de la propiedad superPnrCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSuperPnrCode(String value) {
            this.superPnrCode = value;
        }

        /**
         * Obtiene el valor de la propiedad salesPortalTransactionId.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSalesPortalTransactionId() {
            return salesPortalTransactionId;
        }

        /**
         * Define el valor de la propiedad salesPortalTransactionId.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSalesPortalTransactionId(String value) {
            this.salesPortalTransactionId = value;
        }

        /**
         * Obtiene el valor de la propiedad issueInformation.
         * 
         * @return
         *     possible object is
         *     {@link ValidateFormOfPaymentFraudRQ.SaleInformation.IssueInformation }
         *     
         */
        public ValidateFormOfPaymentFraudRQ.SaleInformation.IssueInformation getIssueInformation() {
            return issueInformation;
        }

        /**
         * Define el valor de la propiedad issueInformation.
         * 
         * @param value
         *     allowed object is
         *     {@link ValidateFormOfPaymentFraudRQ.SaleInformation.IssueInformation }
         *     
         */
        public void setIssueInformation(ValidateFormOfPaymentFraudRQ.SaleInformation.IssueInformation value) {
            this.issueInformation = value;
        }

        /**
         * Obtiene el valor de la propiedad saleType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSaleType() {
            return saleType;
        }

        /**
         * Define el valor de la propiedad saleType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSaleType(String value) {
            this.saleType = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="fopTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="useMultipleFormsOfPayment" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
         *         &lt;element name="useRedemptionVoucher" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
         *         &lt;element name="ExchangeTicketInformation" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="useMultipleExchangeTickets" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
         *                   &lt;element name="issuerIsoCityCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="issueDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="ticketNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="msrNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="msrTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "fopTypeCode",
            "useMultipleFormsOfPayment",
            "useRedemptionVoucher",
            "exchangeTicketInformation"
        })
        public static class FormOfPaymentInformation {

            protected String fopTypeCode;
            protected Boolean useMultipleFormsOfPayment;
            protected Boolean useRedemptionVoucher;
            @XmlElement(name = "ExchangeTicketInformation")
            protected ValidateFormOfPaymentFraudRQ.SaleInformation.FormOfPaymentInformation.ExchangeTicketInformation exchangeTicketInformation;

            /**
             * Obtiene el valor de la propiedad fopTypeCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFopTypeCode() {
                return fopTypeCode;
            }

            /**
             * Define el valor de la propiedad fopTypeCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFopTypeCode(String value) {
                this.fopTypeCode = value;
            }

            /**
             * Obtiene el valor de la propiedad useMultipleFormsOfPayment.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isUseMultipleFormsOfPayment() {
                return useMultipleFormsOfPayment;
            }

            /**
             * Define el valor de la propiedad useMultipleFormsOfPayment.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setUseMultipleFormsOfPayment(Boolean value) {
                this.useMultipleFormsOfPayment = value;
            }

            /**
             * Obtiene el valor de la propiedad useRedemptionVoucher.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isUseRedemptionVoucher() {
                return useRedemptionVoucher;
            }

            /**
             * Define el valor de la propiedad useRedemptionVoucher.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setUseRedemptionVoucher(Boolean value) {
                this.useRedemptionVoucher = value;
            }

            /**
             * Obtiene el valor de la propiedad exchangeTicketInformation.
             * 
             * @return
             *     possible object is
             *     {@link ValidateFormOfPaymentFraudRQ.SaleInformation.FormOfPaymentInformation.ExchangeTicketInformation }
             *     
             */
            public ValidateFormOfPaymentFraudRQ.SaleInformation.FormOfPaymentInformation.ExchangeTicketInformation getExchangeTicketInformation() {
                return exchangeTicketInformation;
            }

            /**
             * Define el valor de la propiedad exchangeTicketInformation.
             * 
             * @param value
             *     allowed object is
             *     {@link ValidateFormOfPaymentFraudRQ.SaleInformation.FormOfPaymentInformation.ExchangeTicketInformation }
             *     
             */
            public void setExchangeTicketInformation(ValidateFormOfPaymentFraudRQ.SaleInformation.FormOfPaymentInformation.ExchangeTicketInformation value) {
                this.exchangeTicketInformation = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="useMultipleExchangeTickets" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
             *         &lt;element name="issuerIsoCityCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="issueDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="ticketNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="msrNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="msrTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "useMultipleExchangeTickets",
                "issuerIsoCityCode",
                "issueDate",
                "ticketNumber",
                "msrNumber",
                "msrTypeCode"
            })
            public static class ExchangeTicketInformation {

                protected Boolean useMultipleExchangeTickets;
                protected String issuerIsoCityCode;
                protected String issueDate;
                protected String ticketNumber;
                protected String msrNumber;
                protected String msrTypeCode;

                /**
                 * Obtiene el valor de la propiedad useMultipleExchangeTickets.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isUseMultipleExchangeTickets() {
                    return useMultipleExchangeTickets;
                }

                /**
                 * Define el valor de la propiedad useMultipleExchangeTickets.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setUseMultipleExchangeTickets(Boolean value) {
                    this.useMultipleExchangeTickets = value;
                }

                /**
                 * Obtiene el valor de la propiedad issuerIsoCityCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getIssuerIsoCityCode() {
                    return issuerIsoCityCode;
                }

                /**
                 * Define el valor de la propiedad issuerIsoCityCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setIssuerIsoCityCode(String value) {
                    this.issuerIsoCityCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad issueDate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getIssueDate() {
                    return issueDate;
                }

                /**
                 * Define el valor de la propiedad issueDate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setIssueDate(String value) {
                    this.issueDate = value;
                }

                /**
                 * Obtiene el valor de la propiedad ticketNumber.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTicketNumber() {
                    return ticketNumber;
                }

                /**
                 * Define el valor de la propiedad ticketNumber.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTicketNumber(String value) {
                    this.ticketNumber = value;
                }

                /**
                 * Obtiene el valor de la propiedad msrNumber.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMsrNumber() {
                    return msrNumber;
                }

                /**
                 * Define el valor de la propiedad msrNumber.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMsrNumber(String value) {
                    this.msrNumber = value;
                }

                /**
                 * Obtiene el valor de la propiedad msrTypeCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMsrTypeCode() {
                    return msrTypeCode;
                }

                /**
                 * Define el valor de la propiedad msrTypeCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMsrTypeCode(String value) {
                    this.msrTypeCode = value;
                }

            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="issueDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *       &lt;attribute name="isIssue" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "issueDate"
        })
        public static class IssueInformation {

            protected String issueDate;
            @XmlAttribute(name = "isIssue", required = true)
            protected boolean isIssue;

            /**
             * Obtiene el valor de la propiedad issueDate.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIssueDate() {
                return issueDate;
            }

            /**
             * Define el valor de la propiedad issueDate.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIssueDate(String value) {
                this.issueDate = value;
            }

            /**
             * Obtiene el valor de la propiedad isIssue.
             * 
             */
            public boolean isIsIssue() {
                return isIssue;
            }

            /**
             * Define el valor de la propiedad isIssue.
             * 
             */
            public void setIsIssue(boolean value) {
                this.isIssue = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="stationNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="salesAgentCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="salesOfficeName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="countrySalesOffice" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "stationNumber",
            "salesAgentCode",
            "salesOfficeName",
            "countrySalesOffice"
        })
        public static class SalesOfficeInformation {

            @XmlElement(required = true)
            protected String stationNumber;
            @XmlElement(required = true)
            protected String salesAgentCode;
            @XmlElement(required = true)
            protected String salesOfficeName;
            @XmlElement(required = true)
            protected String countrySalesOffice;

            /**
             * Obtiene el valor de la propiedad stationNumber.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStationNumber() {
                return stationNumber;
            }

            /**
             * Define el valor de la propiedad stationNumber.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStationNumber(String value) {
                this.stationNumber = value;
            }

            /**
             * Obtiene el valor de la propiedad salesAgentCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSalesAgentCode() {
                return salesAgentCode;
            }

            /**
             * Define el valor de la propiedad salesAgentCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSalesAgentCode(String value) {
                this.salesAgentCode = value;
            }

            /**
             * Obtiene el valor de la propiedad salesOfficeName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSalesOfficeName() {
                return salesOfficeName;
            }

            /**
             * Define el valor de la propiedad salesOfficeName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSalesOfficeName(String value) {
                this.salesOfficeName = value;
            }

            /**
             * Obtiene el valor de la propiedad countrySalesOffice.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCountrySalesOffice() {
                return countrySalesOffice;
            }

            /**
             * Define el valor de la propiedad countrySalesOffice.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCountrySalesOffice(String value) {
                this.countrySalesOffice = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="defaultIsoCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="grandTotalAmount"&gt;
         *           &lt;complexType&gt;
         *             &lt;simpleContent&gt;
         *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
         *                 &lt;attribute name="isoCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *               &lt;/extension&gt;
         *             &lt;/simpleContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="fareClass" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="Items"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="Item" maxOccurs="unbounded"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element name="unitPrice"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;simpleContent&gt;
         *                                   &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
         *                                     &lt;attribute name="isoCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *                                   &lt;/extension&gt;
         *                                 &lt;/simpleContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                             &lt;element name="quantity" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
         *                             &lt;element name="productCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                             &lt;element name="productName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                             &lt;element name="PassengerInformation"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;extension base="{http://entities.latam.com/v3_0}PassengerBookingType"&gt;
         *                                     &lt;sequence&gt;
         *                                       &lt;element name="phoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                                       &lt;element name="secondaryPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                                       &lt;element name="MemberInformations" minOccurs="0"&gt;
         *                                         &lt;complexType&gt;
         *                                           &lt;complexContent&gt;
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                               &lt;sequence&gt;
         *                                                 &lt;element name="MemberInformation" maxOccurs="unbounded"&gt;
         *                                                   &lt;complexType&gt;
         *                                                     &lt;complexContent&gt;
         *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                                         &lt;sequence&gt;
         *                                                           &lt;element name="memberNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                                                           &lt;element name="loyaltyProgramName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                                                           &lt;element name="memberCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                                                           &lt;element name="admissionDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                                                         &lt;/sequence&gt;
         *                                                       &lt;/restriction&gt;
         *                                                     &lt;/complexContent&gt;
         *                                                   &lt;/complexType&gt;
         *                                                 &lt;/element&gt;
         *                                               &lt;/sequence&gt;
         *                                             &lt;/restriction&gt;
         *                                           &lt;/complexContent&gt;
         *                                         &lt;/complexType&gt;
         *                                       &lt;/element&gt;
         *                                     &lt;/sequence&gt;
         *                                   &lt;/extension&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                           &lt;/sequence&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "id",
            "defaultIsoCurrencyCode",
            "grandTotalAmount",
            "fareClass",
            "items"
        })
        public static class ShoppingCartInformation {

            @XmlElement(required = true)
            protected String id;
            @XmlElement(required = true)
            protected String defaultIsoCurrencyCode;
            @XmlElement(required = true)
            protected ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.GrandTotalAmount grandTotalAmount;
            @XmlElement(required = true)
            protected String fareClass;
            @XmlElement(name = "Items", required = true)
            protected ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items items;

            /**
             * Obtiene el valor de la propiedad id.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getId() {
                return id;
            }

            /**
             * Define el valor de la propiedad id.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setId(String value) {
                this.id = value;
            }

            /**
             * Obtiene el valor de la propiedad defaultIsoCurrencyCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDefaultIsoCurrencyCode() {
                return defaultIsoCurrencyCode;
            }

            /**
             * Define el valor de la propiedad defaultIsoCurrencyCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDefaultIsoCurrencyCode(String value) {
                this.defaultIsoCurrencyCode = value;
            }

            /**
             * Obtiene el valor de la propiedad grandTotalAmount.
             * 
             * @return
             *     possible object is
             *     {@link ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.GrandTotalAmount }
             *     
             */
            public ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.GrandTotalAmount getGrandTotalAmount() {
                return grandTotalAmount;
            }

            /**
             * Define el valor de la propiedad grandTotalAmount.
             * 
             * @param value
             *     allowed object is
             *     {@link ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.GrandTotalAmount }
             *     
             */
            public void setGrandTotalAmount(ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.GrandTotalAmount value) {
                this.grandTotalAmount = value;
            }

            /**
             * Obtiene el valor de la propiedad fareClass.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFareClass() {
                return fareClass;
            }

            /**
             * Define el valor de la propiedad fareClass.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFareClass(String value) {
                this.fareClass = value;
            }

            /**
             * Obtiene el valor de la propiedad items.
             * 
             * @return
             *     possible object is
             *     {@link ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items }
             *     
             */
            public ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items getItems() {
                return items;
            }

            /**
             * Define el valor de la propiedad items.
             * 
             * @param value
             *     allowed object is
             *     {@link ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items }
             *     
             */
            public void setItems(ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items value) {
                this.items = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;simpleContent&gt;
             *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
             *       &lt;attribute name="isoCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *     &lt;/extension&gt;
             *   &lt;/simpleContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "value"
            })
            public static class GrandTotalAmount {

                @XmlValue
                protected String value;
                @XmlAttribute(name = "isoCurrencyCode")
                protected String isoCurrencyCode;

                /**
                 * Obtiene el valor de la propiedad value.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getValue() {
                    return value;
                }

                /**
                 * Define el valor de la propiedad value.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setValue(String value) {
                    this.value = value;
                }

                /**
                 * Obtiene el valor de la propiedad isoCurrencyCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getIsoCurrencyCode() {
                    return isoCurrencyCode;
                }

                /**
                 * Define el valor de la propiedad isoCurrencyCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setIsoCurrencyCode(String value) {
                    this.isoCurrencyCode = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="Item" maxOccurs="unbounded"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element name="unitPrice"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;simpleContent&gt;
             *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
             *                           &lt;attribute name="isoCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *                         &lt;/extension&gt;
             *                       &lt;/simpleContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                   &lt;element name="quantity" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
             *                   &lt;element name="productCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *                   &lt;element name="productName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *                   &lt;element name="PassengerInformation"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;extension base="{http://entities.latam.com/v3_0}PassengerBookingType"&gt;
             *                           &lt;sequence&gt;
             *                             &lt;element name="phoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *                             &lt;element name="secondaryPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *                             &lt;element name="MemberInformations" minOccurs="0"&gt;
             *                               &lt;complexType&gt;
             *                                 &lt;complexContent&gt;
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                                     &lt;sequence&gt;
             *                                       &lt;element name="MemberInformation" maxOccurs="unbounded"&gt;
             *                                         &lt;complexType&gt;
             *                                           &lt;complexContent&gt;
             *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                                               &lt;sequence&gt;
             *                                                 &lt;element name="memberNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *                                                 &lt;element name="loyaltyProgramName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *                                                 &lt;element name="memberCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *                                                 &lt;element name="admissionDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *                                               &lt;/sequence&gt;
             *                                             &lt;/restriction&gt;
             *                                           &lt;/complexContent&gt;
             *                                         &lt;/complexType&gt;
             *                                       &lt;/element&gt;
             *                                     &lt;/sequence&gt;
             *                                   &lt;/restriction&gt;
             *                                 &lt;/complexContent&gt;
             *                               &lt;/complexType&gt;
             *                             &lt;/element&gt;
             *                           &lt;/sequence&gt;
             *                         &lt;/extension&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                 &lt;/sequence&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "item"
            })
            public static class Items {

                @XmlElement(name = "Item", required = true)
                protected List<ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item> item;

                /**
                 * Gets the value of the item property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the item property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getItem().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item }
                 * 
                 * 
                 */
                public List<ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item> getItem() {
                    if (item == null) {
                        item = new ArrayList<ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item>();
                    }
                    return this.item;
                }


                /**
                 * <p>Clase Java para anonymous complex type.
                 * 
                 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element name="unitPrice"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;simpleContent&gt;
                 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
                 *                 &lt;attribute name="isoCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                 *               &lt;/extension&gt;
                 *             &lt;/simpleContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *         &lt;element name="quantity" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
                 *         &lt;element name="productCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                 *         &lt;element name="productName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                 *         &lt;element name="PassengerInformation"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;extension base="{http://entities.latam.com/v3_0}PassengerBookingType"&gt;
                 *                 &lt;sequence&gt;
                 *                   &lt;element name="phoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                 *                   &lt;element name="secondaryPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                 *                   &lt;element name="MemberInformations" minOccurs="0"&gt;
                 *                     &lt;complexType&gt;
                 *                       &lt;complexContent&gt;
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                           &lt;sequence&gt;
                 *                             &lt;element name="MemberInformation" maxOccurs="unbounded"&gt;
                 *                               &lt;complexType&gt;
                 *                                 &lt;complexContent&gt;
                 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                                     &lt;sequence&gt;
                 *                                       &lt;element name="memberNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                 *                                       &lt;element name="loyaltyProgramName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                 *                                       &lt;element name="memberCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                 *                                       &lt;element name="admissionDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                 *                                     &lt;/sequence&gt;
                 *                                   &lt;/restriction&gt;
                 *                                 &lt;/complexContent&gt;
                 *                               &lt;/complexType&gt;
                 *                             &lt;/element&gt;
                 *                           &lt;/sequence&gt;
                 *                         &lt;/restriction&gt;
                 *                       &lt;/complexContent&gt;
                 *                     &lt;/complexType&gt;
                 *                   &lt;/element&gt;
                 *                 &lt;/sequence&gt;
                 *               &lt;/extension&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *       &lt;/sequence&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "unitPrice",
                    "quantity",
                    "productCode",
                    "productName",
                    "passengerInformation"
                })
                public static class Item {

                    @XmlElement(required = true)
                    protected ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item.UnitPrice unitPrice;
                    protected Integer quantity;
                    protected String productCode;
                    protected String productName;
                    @XmlElement(name = "PassengerInformation", required = true)
                    protected ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item.PassengerInformation passengerInformation;

                    /**
                     * Obtiene el valor de la propiedad unitPrice.
                     * 
                     * @return
                     *     possible object is
                     *     {@link ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item.UnitPrice }
                     *     
                     */
                    public ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item.UnitPrice getUnitPrice() {
                        return unitPrice;
                    }

                    /**
                     * Define el valor de la propiedad unitPrice.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item.UnitPrice }
                     *     
                     */
                    public void setUnitPrice(ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item.UnitPrice value) {
                        this.unitPrice = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad quantity.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Integer }
                     *     
                     */
                    public Integer getQuantity() {
                        return quantity;
                    }

                    /**
                     * Define el valor de la propiedad quantity.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Integer }
                     *     
                     */
                    public void setQuantity(Integer value) {
                        this.quantity = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad productCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getProductCode() {
                        return productCode;
                    }

                    /**
                     * Define el valor de la propiedad productCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setProductCode(String value) {
                        this.productCode = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad productName.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getProductName() {
                        return productName;
                    }

                    /**
                     * Define el valor de la propiedad productName.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setProductName(String value) {
                        this.productName = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad passengerInformation.
                     * 
                     * @return
                     *     possible object is
                     *     {@link ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item.PassengerInformation }
                     *     
                     */
                    public ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item.PassengerInformation getPassengerInformation() {
                        return passengerInformation;
                    }

                    /**
                     * Define el valor de la propiedad passengerInformation.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item.PassengerInformation }
                     *     
                     */
                    public void setPassengerInformation(ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item.PassengerInformation value) {
                        this.passengerInformation = value;
                    }


                    /**
                     * <p>Clase Java para anonymous complex type.
                     * 
                     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;extension base="{http://entities.latam.com/v3_0}PassengerBookingType"&gt;
                     *       &lt;sequence&gt;
                     *         &lt;element name="phoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                     *         &lt;element name="secondaryPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                     *         &lt;element name="MemberInformations" minOccurs="0"&gt;
                     *           &lt;complexType&gt;
                     *             &lt;complexContent&gt;
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *                 &lt;sequence&gt;
                     *                   &lt;element name="MemberInformation" maxOccurs="unbounded"&gt;
                     *                     &lt;complexType&gt;
                     *                       &lt;complexContent&gt;
                     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *                           &lt;sequence&gt;
                     *                             &lt;element name="memberNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                     *                             &lt;element name="loyaltyProgramName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                     *                             &lt;element name="memberCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                     *                             &lt;element name="admissionDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                     *                           &lt;/sequence&gt;
                     *                         &lt;/restriction&gt;
                     *                       &lt;/complexContent&gt;
                     *                     &lt;/complexType&gt;
                     *                   &lt;/element&gt;
                     *                 &lt;/sequence&gt;
                     *               &lt;/restriction&gt;
                     *             &lt;/complexContent&gt;
                     *           &lt;/complexType&gt;
                     *         &lt;/element&gt;
                     *       &lt;/sequence&gt;
                     *     &lt;/extension&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "phoneNumber",
                        "secondaryPhoneNumber",
                        "memberInformations"
                    })
                    public static class PassengerInformation
                        extends PassengerBookingType
                    {

                        protected String phoneNumber;
                        protected String secondaryPhoneNumber;
                        @XmlElement(name = "MemberInformations")
                        protected ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item.PassengerInformation.MemberInformations memberInformations;

                        /**
                         * Obtiene el valor de la propiedad phoneNumber.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getPhoneNumber() {
                            return phoneNumber;
                        }

                        /**
                         * Define el valor de la propiedad phoneNumber.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setPhoneNumber(String value) {
                            this.phoneNumber = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad secondaryPhoneNumber.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getSecondaryPhoneNumber() {
                            return secondaryPhoneNumber;
                        }

                        /**
                         * Define el valor de la propiedad secondaryPhoneNumber.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setSecondaryPhoneNumber(String value) {
                            this.secondaryPhoneNumber = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad memberInformations.
                         * 
                         * @return
                         *     possible object is
                         *     {@link ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item.PassengerInformation.MemberInformations }
                         *     
                         */
                        public ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item.PassengerInformation.MemberInformations getMemberInformations() {
                            return memberInformations;
                        }

                        /**
                         * Define el valor de la propiedad memberInformations.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item.PassengerInformation.MemberInformations }
                         *     
                         */
                        public void setMemberInformations(ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item.PassengerInformation.MemberInformations value) {
                            this.memberInformations = value;
                        }


                        /**
                         * <p>Clase Java para anonymous complex type.
                         * 
                         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                         * 
                         * <pre>
                         * &lt;complexType&gt;
                         *   &lt;complexContent&gt;
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                         *       &lt;sequence&gt;
                         *         &lt;element name="MemberInformation" maxOccurs="unbounded"&gt;
                         *           &lt;complexType&gt;
                         *             &lt;complexContent&gt;
                         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                         *                 &lt;sequence&gt;
                         *                   &lt;element name="memberNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                         *                   &lt;element name="loyaltyProgramName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                         *                   &lt;element name="memberCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                         *                   &lt;element name="admissionDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                         *                 &lt;/sequence&gt;
                         *               &lt;/restriction&gt;
                         *             &lt;/complexContent&gt;
                         *           &lt;/complexType&gt;
                         *         &lt;/element&gt;
                         *       &lt;/sequence&gt;
                         *     &lt;/restriction&gt;
                         *   &lt;/complexContent&gt;
                         * &lt;/complexType&gt;
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "memberInformation"
                        })
                        public static class MemberInformations {

                            @XmlElement(name = "MemberInformation", required = true)
                            protected List<ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item.PassengerInformation.MemberInformations.MemberInformation> memberInformation;

                            /**
                             * Gets the value of the memberInformation property.
                             * 
                             * <p>
                             * This accessor method returns a reference to the live list,
                             * not a snapshot. Therefore any modification you make to the
                             * returned list will be present inside the JAXB object.
                             * This is why there is not a <CODE>set</CODE> method for the memberInformation property.
                             * 
                             * <p>
                             * For example, to add a new item, do as follows:
                             * <pre>
                             *    getMemberInformation().add(newItem);
                             * </pre>
                             * 
                             * 
                             * <p>
                             * Objects of the following type(s) are allowed in the list
                             * {@link ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item.PassengerInformation.MemberInformations.MemberInformation }
                             * 
                             * 
                             */
                            public List<ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item.PassengerInformation.MemberInformations.MemberInformation> getMemberInformation() {
                                if (memberInformation == null) {
                                    memberInformation = new ArrayList<ValidateFormOfPaymentFraudRQ.SaleInformation.ShoppingCartInformation.Items.Item.PassengerInformation.MemberInformations.MemberInformation>();
                                }
                                return this.memberInformation;
                            }


                            /**
                             * <p>Clase Java para anonymous complex type.
                             * 
                             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                             * 
                             * <pre>
                             * &lt;complexType&gt;
                             *   &lt;complexContent&gt;
                             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                             *       &lt;sequence&gt;
                             *         &lt;element name="memberNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                             *         &lt;element name="loyaltyProgramName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                             *         &lt;element name="memberCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                             *         &lt;element name="admissionDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
                             *       &lt;/sequence&gt;
                             *     &lt;/restriction&gt;
                             *   &lt;/complexContent&gt;
                             * &lt;/complexType&gt;
                             * </pre>
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "memberNumber",
                                "loyaltyProgramName",
                                "memberCategory",
                                "admissionDate"
                            })
                            public static class MemberInformation {

                                protected String memberNumber;
                                protected String loyaltyProgramName;
                                protected String memberCategory;
                                protected String admissionDate;

                                /**
                                 * Obtiene el valor de la propiedad memberNumber.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getMemberNumber() {
                                    return memberNumber;
                                }

                                /**
                                 * Define el valor de la propiedad memberNumber.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setMemberNumber(String value) {
                                    this.memberNumber = value;
                                }

                                /**
                                 * Obtiene el valor de la propiedad loyaltyProgramName.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getLoyaltyProgramName() {
                                    return loyaltyProgramName;
                                }

                                /**
                                 * Define el valor de la propiedad loyaltyProgramName.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setLoyaltyProgramName(String value) {
                                    this.loyaltyProgramName = value;
                                }

                                /**
                                 * Obtiene el valor de la propiedad memberCategory.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getMemberCategory() {
                                    return memberCategory;
                                }

                                /**
                                 * Define el valor de la propiedad memberCategory.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setMemberCategory(String value) {
                                    this.memberCategory = value;
                                }

                                /**
                                 * Obtiene el valor de la propiedad admissionDate.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getAdmissionDate() {
                                    return admissionDate;
                                }

                                /**
                                 * Define el valor de la propiedad admissionDate.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setAdmissionDate(String value) {
                                    this.admissionDate = value;
                                }

                            }

                        }

                    }


                    /**
                     * <p>Clase Java para anonymous complex type.
                     * 
                     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;simpleContent&gt;
                     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
                     *       &lt;attribute name="isoCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
                     *     &lt;/extension&gt;
                     *   &lt;/simpleContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "value"
                    })
                    public static class UnitPrice {

                        @XmlValue
                        protected String value;
                        @XmlAttribute(name = "isoCurrencyCode")
                        protected String isoCurrencyCode;

                        /**
                         * Obtiene el valor de la propiedad value.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getValue() {
                            return value;
                        }

                        /**
                         * Define el valor de la propiedad value.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setValue(String value) {
                            this.value = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad isoCurrencyCode.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getIsoCurrencyCode() {
                            return isoCurrencyCode;
                        }

                        /**
                         * Define el valor de la propiedad isoCurrencyCode.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setIsoCurrencyCode(String value) {
                            this.isoCurrencyCode = value;
                        }

                    }

                }

            }

        }

    }

}
